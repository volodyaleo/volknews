# Installing depencies

Install npm and tizen studio, then call in project directory:

```bash
npm install .
```

# Run in emulator steps

Build for production:

```bash
npm run prod
```

Pack in package

```bash
npm run package
```

Run emulator

```bash
npm run emulator
```

Install on target

```bash
npm run install
```

Run program

```bash
npm run run
```

# Setting up tizen-util

## Mac

Mac has wrong settings in  `node_modules/tizen-utils/lip/tizen.json`. So, correct settings is:
```
{
    "emulator": "~/tizen-studio/tools/emulator/bin/emulator-manager",
    "cli": "~/tizen-studio/tools/ide/bin/tizen"
}
```
