export default [
    {
        path: '/newslist/:id',
        asyncComponent: () => import('./view/newslist.svelte')
    },
    {
        path: '/article/:id',
        asyncComponent: () => import('./view/article.svelte')
    },
    {
        path: '/siteslist',
        asyncComponent: () => import('./view/siteslist.svelte')
    }
];
