const RSS_URL = "https://lenta.ru/rss";

const Parser = require("rss-parser");
let parser = new Parser();

module.exports.name = "Lenta.ru";

module.exports.icon = require("../../icons/lenta-icon.png");

module.exports.getNews = function(baseuri, limit) {
    return parser.parseURL(RSS_URL).then((val) => val.items);
}
