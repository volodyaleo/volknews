const RSS_URL = "https://tjournal.ru/rss/news";

const Parser = require("rss-parser");
let parser = new Parser();

module.exports.name = "TJournal";

module.exports.icon = require("../../icons/tj-icon.png");

module.exports.getNews = function(baseuri, limit) {
    return parser.parseURL(RSS_URL).then((val) => val.items);
}
