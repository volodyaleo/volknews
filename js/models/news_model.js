const models = [
    require("./lenta.ru"),
    require("./tjournal.ru"),
    require("./vc.ru")    
];

// default model
let model = models[0];

let cache = [];

module.exports.getNews = function() {
    if (cache.length == 0)
        return model.getNews().then((newsArray) => {
            cache = newsArray;

            return cache;
        });
    else
        return new Promise((resolve, reject) => {
            resolve(cache);
        });
};

module.exports.getCached = function(index) {
    return new Promise((resolve, reject) => {
        if (index > cache.length)
            reject(-1);

        resolve(cache[index]);
    })
};

module.exports.getModelsList = function() {
    return models;
}

module.exports.setModel = function(m) {
    cache = [];

    model = m;
}

module.exports.setModelByIdx = function(idx) {
    cache = [];

    model = models[idx];
}

module.exports.clearCache = function() {
    cache = [];
}
