const RSS_URL = "https://vc.ru/rss/all";

const Parser = require("rss-parser");
let parser = new Parser();

module.exports.name = "vc.ru";

module.exports.icon = require("../../icons/vc-icon.png");

module.exports.getNews = function(baseuri, limit) {
    return parser.parseURL(RSS_URL).then((val) => val.items);
}
