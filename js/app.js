import Framework7 from 'framework7/framework7-lite.esm.bundle.js';
import Framework7Svelte from 'framework7-svelte';

Framework7.use(Framework7Svelte)

import 'framework7/css/framework7.bundle.css';
import App from './App.svelte';

import '../css/theme.css';

export default new App({
	target: document.body
});
