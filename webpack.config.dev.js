const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const webpack = require('webpack');

module.exports = {
    mode: 'development',
    entry: './js/app.js',
    output: {
        path: path.resolve(__dirname, './dist'),
        filename: 'main.js'
    },
    module: {
        rules: [
            {
                test: /\.(js)$/,
                exclude: /(node_modules|bower_components)/,
                use: 'babel-loader'
            },
            {
                test: /\.css$/i,
                use: ['style-loader', 'css-loader'],
            },
            {
                test: /\.(png|jpg|svg|ttf|eot|woff|woff2)$/,
                loader: 'file-loader',
                options: {
                    name: '[path][name].[ext]'        
                }
            },
            {
                test: /\.svelte$/,
				use: {
					loader: 'svelte-loader',
					options: {
						emitCss: true,
						hotReload: true
					}
                }
            }
        ]
    },
    plugins: [    
        new CopyPlugin({
            patterns: [
                {
                    from: './config.xml',
                    to: './'
                },
                {
                    from: './bg.png',
                    to: './'
                },
                {
                    from: './icon.png',
                    to: './'
                },
            ],
        }),
        new HtmlWebpackPlugin({
            filename: 'index.html'
        }),
        // TODO: Mock tizen
        // new webpack.DefinePlugin({
        // 		tizen: {
        // 				time: {
        // 					setTimezoneChangeListener: function() {
								
        // 					},
        // 					getCurrentDateTime: function() {
		// 						return new Date();
		// 				}
        // 				}
        // 		}
        // })
    ]
};
