const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const webpack = require('webpack');

module.exports = {
    mode: 'production',
    entry: './js/app.js',
    output: {
        path: path.resolve(__dirname, './dist'),
        filename: 'main.js'
    },
    module: {
        rules: [
            {
                test: /\.(js)$/,
                exclude: /(node_modules|bower_components)/,
                use: 'babel-loader'
            },
            {
                test: /\.css$/i,
                use: [
                    'style-loader',
                    { 
                        loader: 'css-loader', 
                    },
                    {
                        loader: 'postcss-loader',
                        options: {
                            ident: 'postcss',
                            sourceMap: true,
                            plugins: [
                                require('autoprefixer')({}),
                                require('cssnano')({ preset: 'default' })
                            ]
                        }
                    }
                ],
            },
            {
                test: /\.(png|jpg|svg|ttf|eot|woff|woff2)$/,
                loader: 'file-loader',
                options: {
                    name: '[path][name].[ext]'        
                }
            },
            {
                test: /\.svelte$/,
				use: [
                    {
                        loader: 'babel-loader'
                    },
                    {
                        loader: 'svelte-loader',
                        options: {
                            emitCss: true,
                            hotReload: true
                        }
                    }
                ]
            }
        ]
    },
    plugins: [    
       new CopyPlugin({
           patterns: [
                {
                    from: './config.xml',
                    to: './'
                },
                {
                    from: './bg.png',
                    to: './'
                },
                {
                    from: './icon.png',
                    to: './'
                },
           ],
       }),
        new HtmlWebpackPlugin({
            filename: 'index.html',
            minify: {
                collapseWhitespace: true
            }
        })
    ]
};
